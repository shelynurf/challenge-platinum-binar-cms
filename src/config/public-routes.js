import React from "react";
import Login from "../pages/login";
import { useRoutes } from "react-router";

const publicRoutes = (props) => {
	return [
		{
			index: true,
			path: "/",
			element: <Login {...props} title="Login" />,
		},
		{
			index: true,
			path: "/login",
			element: <Login {...props} title="Login" />,
		},
		{ index: true, path: "*", element: <div>Halaman Not Found</div> },
	];
};

const PublicRoutes = (props) => {
	const routes = useRoutes(publicRoutes(props));
	return routes;
};

export default PublicRoutes;
