import React from "react";
import Dashboard from "../pages/dashboard";
import Cars from "../pages/cars";
import Login from "../pages/login";
import { useRoutes } from "react-router";

const privateRoutes = (props) => {
	return [
		{
			index: true,
			path: "/",
			element: <Dashboard {...props} title="Dashboard" />,
		},
		{
			index: true,
			path: "/cars",
			element: <Cars {...props} title="Cars" />,
		},

		{ index: true, path: "*", element: <div>Halaman Not Found</div> },
	];
};

const PrivateRoutes = (props) => {
	const routes = useRoutes(privateRoutes(props));
	return routes;
};

export default PrivateRoutes;
