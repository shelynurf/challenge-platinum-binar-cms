import React from "react";
import Layout from "../layout";
import PublicRoutes from "./public-routes";
import { token } from "./token";

const AuthProvider = () => {
	const isLoggedIn = token ? true : false;
	return isLoggedIn ? <Layout /> : <PublicRoutes />;
};

export default AuthProvider;
