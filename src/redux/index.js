import { applyMiddleware, compose, createStore } from "redux";
import rootReducer from "./reducers/index";
import { createLogger } from "redux-logger";

let store = null;
store = createStore(rootReducer, compose(applyMiddleware(createLogger())));

export default store;
