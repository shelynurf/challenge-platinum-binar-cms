const initialState = {
	open: false,
};

export const OpenSidebar = (state = initialState, action) => {
	switch (action.type) {
		case "openSidebar":
			return {
				...state,
				open: action.payload,
			};

		default:
			return {
				...state,
			};
	}
};
