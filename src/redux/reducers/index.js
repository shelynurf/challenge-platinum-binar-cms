import { combineReducers } from "redux";
import { OpenSidebar } from "./openSidebar";

const rootReducer = combineReducers({
	OpenSidebar,
});

export default rootReducer;
