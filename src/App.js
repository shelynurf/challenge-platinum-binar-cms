import "../src/assets/css/App.css";

import AuthProvider from "./config/auth-provider";

function App() {
	return <AuthProvider />;
}

export default App;
