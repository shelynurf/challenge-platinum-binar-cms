import React, { useEffect, useState } from "react";
import { Sidebar, Menu, MenuItem, SubMenu } from "react-pro-sidebar";
import HomeIcon from "../../assets/images/fi_home.png";
import CarsIcon from "../../assets/images/fi_truck.png";
import { Drawer } from "@mui/material";

const Sidebars = () => {
	const [state, setState] = useState(false);

	const toggleDrawer = (open) => (event) => {
		if (
			event.type === "keydown" &&
			(event.key === "Tab" || event.key === "Shift")
		) {
			return;
		}

		setState(open);
	};
	return (
		<div>
			{/* <Sidebar>
				<Menu>
					<SubMenu label="Charts">
						<MenuItem> Pie charts </MenuItem>
						<MenuItem> Line charts </MenuItem>
					</SubMenu>

					<MenuItem> Documentation </MenuItem>
					<MenuItem> Calendar </MenuItem>
				</Menu>
			</Sidebar> */}
			{/* <Drawer anchor={left} open={state} onClose={toggleDrawer(false)}></Drawer> */}
		</div>
	);
};

export default Sidebars;
