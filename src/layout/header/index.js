import {
	ArrowDropDownOutlined,
	ChevronLeft,
	ChevronRight,
	HomeOutlined,
} from "@mui/icons-material";
import {
	AppBar,
	Avatar,
	Box,
	Drawer,
	IconButton,
	List,
	ListItem,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	MenuItem,
	Toolbar,
} from "@mui/material";
import Menu from "@mui/material/Menu";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import { styled, useTheme } from "@mui/material/styles";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import HomeIcon from "../../assets/images/fi_home.png";
import CarsIcon from "../../assets/images/fi_truck.png";
import PrivateRoutes from "../../config/private-routes";

const Header = () => {
	const [anchorEl, setAnchorEl] = useState(null);
	const handleMenu = (e) => {
		console.log(e.currentTarget);
		setAnchorEl(e.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	// const [state, setState] = useState(false);

	// const toggleDrawer = (open) => (event) => {
	// 	if (
	// 		event.type === "keydown" &&
	// 		(event.key === "Tab" || event.key === "Shift")
	// 	) {
	// 		return;
	// 	}

	// 	setState(open);
	// };

	const drawerWidth = 240;

	const openedMixin = (theme) => ({
		width: drawerWidth,
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
		overflowX: "hidden",
	});

	const closedMixin = (theme) => ({
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		overflowX: "hidden",
		width: `calc(${theme.spacing(7)} + 1px)`,
		[theme.breakpoints.up("sm")]: {
			width: `calc(${theme.spacing(8)} + 1px)`,
		},
	});

	const DrawerHeader = styled("div")(({ theme }) => ({
		display: "flex",
		alignItems: "center",
		justifyContent: "flex-end",
		padding: theme.spacing(0, 1),
		// necessary for content to be below app bar
		...theme.mixins.toolbar,
	}));

	const AppBar = styled(MuiAppBar, {
		shouldForwardProp: (prop) => prop !== "open",
	})(({ theme, open }) => ({
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(["width", "margin"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		...(open && {
			marginLeft: drawerWidth,
			width: `calc(100% - ${drawerWidth}px)`,
			transition: theme.transitions.create(["width", "margin"], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen,
			}),
		}),
	}));

	const Drawer = styled(MuiDrawer, {
		shouldForwardProp: (prop) => prop !== "open",
	})(({ theme, open }) => ({
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: "nowrap",
		boxSizing: "border-box",
		...(open && {
			...openedMixin(theme),
			"& .MuiDrawer-paper": openedMixin(theme),
		}),
		...(!open && {
			...closedMixin(theme),
			"& .MuiDrawer-paper": closedMixin(theme),
		}),
	}));

	const theme = useTheme();
	const [open, setOpen] = useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	const navigate = useNavigate();
	return (
		<div className="header d-flex">
			<AppBar color="inherit" position="fixed" open={open}>
				<Toolbar className="d-flex justify-content-between">
					<IconButton
						size="small"
						edge="start"
						color="inherit"
						aria-label="open drawer"
						sx={{ mr: 2 }}
						// onClick={toggleDrawer(true)}
						onClick={handleDrawerOpen}>
						<i className="fa fa-bars"></i>
					</IconButton>
					<div className="d-flex align-items-center gap-4">
						{/* <div className="d-flex">
							<input className="form-control" placeholder="Search"></input>
							<button
								className="btn btn-outline-primary"
								style={{ color: "#0D28A6", borderColor: "#0D28A6" }}>
								Search
							</button>
						</div> */}
						<div class="input-group">
							<input
								type="text"
								class="form-control"
								placeholder="Search"
								// aria-label="Recipient's username"
								// aria-describedby="button-addon2"
							/>
							<button
								class="btn btn-outline-primary"
								type="button"
								// id="button-addon2"
								style={{ color: "#0D28A6", borderColor: "#0D28A6" }}>
								Search
							</button>
						</div>

						<div className="d-flex align-items-center gap-2">
							<Avatar>U</Avatar>
							<span className="font-14-thin">Unis Badri</span>
							<IconButton
								size="large"
								aria-label="account of current user"
								aria-controls="menu-appbar"
								aria-haspopup="true"
								onClick={handleMenu}
								color="inherit">
								{/* <AccountCircle /> */}

								<ArrowDropDownOutlined />
							</IconButton>
							<Menu
								id="menu-appbar"
								anchorEl={anchorEl}
								// anchorOrigin={{
								// 	vertical: "top",
								// 	horizontal: "right",
								// }}
								keepMounted
								transformOrigin={{
									vertical: "top",
									// horizontal: "right",
								}}
								open={Boolean(anchorEl)}
								onClose={handleClose}>
								<MenuItem>Profile</MenuItem>
								<MenuItem
									onClick={() => {
										localStorage.removeItem("ACCESS_TOKEN");
										window.location.replace("/");
									}}>
									Sign Out
								</MenuItem>
							</Menu>
						</div>
					</div>
				</Toolbar>
			</AppBar>
			{/* <button>
				<i className="fa fa-bars" onClick={() => openSideBar()}></i>
			</button> */}
			<Drawer
				anchor="left"
				open={open}
				// onClose={toggleDrawer(false)}
				color="primary"
				variant="permanent">
				<DrawerHeader>
					<IconButton onClick={handleDrawerClose}>
						{theme.direction === "rtl" ? <ChevronRight /> : <ChevronLeft />}
					</IconButton>
				</DrawerHeader>
				<List>
					<ListItem disablePadding sx={{ display: "block" }}>
						<ListItemButton
							sx={{
								minHeight: 48,
								justifyContent: open ? "initial" : "center",
								px: 2.5,
							}}
							onClick={() => navigate("/")}>
							<ListItemIcon
								sx={{
									minWidth: 0,
									mr: open ? 3 : "auto",
									justifyContent: "center",
								}}>
								{/* <img src={HomeIcon} /> */}
								<HomeOutlined />
							</ListItemIcon>
							<ListItemText sx={{ opacity: open ? 1 : 0 }}>
								Dashboard
							</ListItemText>
						</ListItemButton>
						<ListItemButton
							sx={{
								minHeight: 48,
								justifyContent: open ? "initial" : "center",
								px: 2.5,
							}}
							onClick={() => navigate("/cars")}>
							<ListItemIcon
								sx={{
									minWidth: 0,
									mr: open ? 3 : "auto",
									justifyContent: "center",
								}}>
								{/* <img src={HomeIcon} /> */}
								<span class="material-icons">local_shipping</span>
							</ListItemIcon>
							<ListItemText sx={{ opacity: open ? 1 : 0 }}>Cars</ListItemText>
						</ListItemButton>
					</ListItem>
					{/* <p>Dashboard</p>
					<p>Car List</p> */}
				</List>
			</Drawer>

			<Box component="main" sx={{ flexGrow: 1, p: 3 }}>
				<PrivateRoutes />
			</Box>
		</div>
	);
};

export default Header;
