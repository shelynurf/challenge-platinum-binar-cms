import React from "react";
import Header from "./header";
import Login from "../pages/login";

import { useLocation } from "react-router";
import PublicRoutes from "../config/public-routes";
import PrivateRoutes from "../config/private-routes";
import Sidebars from "./sidebar";

// const LoginRoutes = () => {
// 	return <PublicRoutes />;
// };

const Layout = () => {
	return (
		<div>
			{/* <Sidebars /> */}

			<Header />
			{/* <PrivateRoutes /> */}
		</div>
	);
};
// const Layout = () => {
// 	const { pathname } = useLocation();
// 	return (
// 		<div>{pathname !== "/login" ? <InlineRoutes /> : <LoginRoutes />}</div>
// 	);
// };

export default Layout;
