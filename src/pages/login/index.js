import React, { useState } from "react";
import BannerAdmin from "../../assets/images/image 2 (1).png";
import { Form } from "reactstrap";
import { Alert } from "@mui/material";
import { Services } from "../../config/api-middleware";

const Login = () => {
	const [state, setState] = useState({
		email: "",
		password: "",
	});

	const handleChange = (e) => {
		const { name, value } = e.target;
		setState((prev) => ({
			...prev,
			[name]: value,
		}));
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		Services()
			.post("https://bootcamp-rent-cars.herokuapp.com/admin/auth/login", {
				...state,
			})
			.then((response) => {
				console.log("response", response);
				localStorage.setItem("ACCESS_TOKEN", response.data.access_token);
				window.location.replace("/");
			})
			.catch((err) => console.log(err.response.data.message));
	};

	return (
		<div className="d-flex">
			<div className="col-8">
				<img src={BannerAdmin} style={{ width: "100%" }}></img>
			</div>
			<div className="col-4 d-flex align-items-center justify-content-center bg-white">
				<div className="d-flex flex-column gap-3">
					<div className="logo"></div>
					<h1 className="font-24">Welcome, Admin BCR</h1>
					{/* <div className="w-75">
						<Alert severity="error">
							Masukkan username dan password yang benar. Perhatikan penggunaan
							huruf kapital.
						</Alert>
					</div> */}
					<Form onSubmit={handleSubmit}>
						<div className="d-grid gap-3">
							<label className="font-14-thin">Email</label>
							<input
								type="text"
								name="email"
								onChange={handleChange}
								className="form-control"
								placeholder="Contoh: johndee@gmail.com"
								required></input>
						</div>
						<div className="d-grid gap-3">
							<label className="font-14-thin">Password</label>
							<input
								type="password"
								name="password"
								onChange={handleChange}
								className="form-control"
								placeholder="6+ karakter"
								required></input>
						</div>
						<div>
							<button className="btn btn-blue w-100 font-14">Sign In</button>
						</div>
					</Form>
				</div>
			</div>
		</div>
	);
};

export default Login;
