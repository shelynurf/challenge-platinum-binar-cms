import { NavigateNext } from "@mui/icons-material";
import { Breadcrumbs, Link } from "@mui/material";
import React from "react";

const Dashboard = () => {
	return (
		<div className="container d-flex gap-5 flex-column py-3">
			<Breadcrumbs separator={<NavigateNext fontSize="small" />}>
				<Link underline="hover" color="inherit" className="font-12">
					Dashboard
				</Link>
				<span className="font-12-thin">Dashboard</span>
			</Breadcrumbs>
			<div className="d-flex flex-column gap-4">
				<div className="d-flex gap-2">
					<div className="selected-page"></div>
					<span className="font-14">Rented Car Data Visualization</span>
				</div>

				<div>
					<span>Month</span>
					<div class="input-group" style={{ width: "fit-content" }}>
						<select
							class="form-select"
							id="inputGroupSelect04"
							aria-label="Example select with button addon">
							<option selected>June - 2022</option>
							<option value="1">One</option>
							<option value="2">Two</option>
							<option value="3">Three</option>
						</select>
						<button
							class="btn btn-primary"
							type="button"
							style={{ background: "#0D28A6", border: "#0D28A6" }}>
							Go
						</button>
					</div>
				</div>
			</div>

			<div>
				<h1 className="font-20">Dashboard</h1>
				<div className="d-flex gap-2">
					<div className="selected-page"></div>
					<span className="font-14">List Order</span>
				</div>
			</div>
		</div>
	);
};

export default Dashboard;
