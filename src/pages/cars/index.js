import {
	DeleteOutline,
	EditOutlined,
	NavigateNext,
	PeopleOutlined,
} from "@mui/icons-material";
import { Breadcrumbs, Link } from "@mui/material";
import React from "react";
import ImageCar from "../../assets/images/image1.png";
import clockIcon from "../../assets/images/fi_clock.png";
import carIcon from "../../assets/images/Beep Beep - Medium Vehicle.png";
import cloudIcon from "../../assets/images/Beep Beep - Clouds.png";

const Cars = () => {
	return (
		<div id="list-car" className="container d-flex gap-5 flex-column py-3">
			<Breadcrumbs separator={<NavigateNext fontSize="small" />}>
				<Link underline="hover" color="inherit" className="font-12">
					Car
				</Link>
				<span className="font-12-thin">List Car</span>
			</Breadcrumbs>
			<div className="d-flex justify-content-between">
				<h1 className="font-20">List Car</h1>
				<button className="btn btn-blue font-14 gap-3">
					<span>+</span>
					<span>Add New Car</span>
				</button>
			</div>
			<div className="d-flex gap-3">
				<button className="btn btn-filter-car">All</button>
				<button className="btn btn-filter-car">2 - 4 people</button>
				<button className="btn btn-filter-car">4 - 6 people</button>
				<button className="btn btn-filter-car">6 - 8 people</button>
			</div>

			<div className="col-4">
				<div className="card">
					<div className="card-body d-grid gap-3">
						<img src={ImageCar}></img>
						<div className="d-grid gap-2">
							<span className="font-14-thin">Nama / Tipe Mobil</span>
							<span className="font-16">Rp 500.000 / hari</span>
							<div className="d-flex gap-2 align-items-center">
								<PeopleOutlined></PeopleOutlined>
								<span className="font-12-thin align-items-center">
									6 - 8 People
								</span>
							</div>
							<div className="d-flex gap-2 align-items-center">
								<div>
									<img src={clockIcon}></img>
								</div>
								<span className="font-14-thin">
									Updated at 4 Apr 2022, 09.00
								</span>
							</div>
						</div>
						<div className="row">
							<div className="col">
								<button
									className="btn btn-delete w-100"
									style={{ color: "#FA2C5A", borderColor: "#FA2C5A" }}>
									<DeleteOutline color="#FA2C5A"></DeleteOutline>
									<span className="font-14">Delete</span>
								</button>
							</div>
							<div className="col">
								<button className="btn btn-edit w-100">
									<EditOutlined />
									<span className="font-14">Edit</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="dialog-box" className="col-4">
				<div className="card">
					<div className="card-body">
						<div className="d-grid justify-content-center">
							<img src={cloudIcon}></img>
							<img src={carIcon}></img>
						</div>
						<div className="d-grid text-center">
							<p className="font-16">Menghapus Data Mobil</p>
							<p className="font-14-thin">
								Setelah dihapus, data mobil tidak dapat dikembalikan. Yakin
								ingin menghapus?
							</p>
						</div>
						<div className="d-flex justify-content-center gap-3">
							<button
								className="btn btn-blue font-14"
								style={{ width: "87px" }}>
								Ya
							</button>
							<button
								className="btn btn-blue font-14"
								style={{
									width: "87px",
									background: "white",
									color: "#0D28A6",
								}}>
								Tidak
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Cars;
